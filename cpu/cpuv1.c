#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>
#include <linux/sched/signal.h>
#include <linux/sched.h>
 

#define PROCFS_NAME "cpuv1_201213192"


struct task_struct *task;          
struct task_struct *task_child;    
struct list_head *list;  

static int OS2_show(struct seq_file *m, void *v){
seq_printf(m, "Carne: 20123192 \n");
seq_printf(m, "Nombre: Hugo Alberto Meoño Boiton \n");
seq_printf(m, "SO: Fedora 29 \n");

seq_printf(m, "Cargando informacion de procesos \n");
seq_printf(m, "PID , PROCESS ,STATE\n");
for_each_process( task ){ 
	seq_printf(m,"PARENT PID: %d PROCESS: %s STATE: %ld",task->pid, task->comm, task->state);
	list_for_each(list, &task->children){                        
 
            task_child = list_entry( list, struct task_struct, sibling );    
     
            seq_printf(m, "\nCHILD OF %s[%d] PID: %d PROCESS: %s STATE: %ld",task->comm, task->pid, 
                task_child->pid, task_child->comm, task_child->state);
        }
        seq_printf(m,"\n-----------------------------------------------------\n"); 
}
return 0;
}

static int OS2_open(struct inode *inode, struct file *file){
return single_open(file, OS2_show, NULL);
}

static const struct file_operations OS2_fops = {
.owner = THIS_MODULE,
.open = OS2_open,
.read = seq_read,
.llseek = seq_lseek,
.release = single_release,
};

static int ver_cpu_init(void){
	printk(KERN_INFO "Cargando modulo cpu.\r\n");
	proc_create(PROCFS_NAME, 0, NULL, &OS2_fops);
	printk(KERN_INFO "Nombre : Hugo Alberto Meoño Boiton \n Carnet : 201213192 \n Completado. Procceso: /proc/%s.\r\n", PROCFS_NAME);
	return 0;
}

static void ver_cpu_exit(void){
        
        printk(KERN_INFO "Modulo CPU Deshabilitado Sistemas Operativos Uno.\r\n");
        remove_proc_entry(PROCFS_NAME, NULL);		        
}

module_init(ver_cpu_init);
module_exit(ver_cpu_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("HB");
MODULE_DESCRIPTION("ejemplo de como menejar cpu");